__version__ = "0.2.0"

from .CourseWatcher import CourseWatcher
from .Configuration import Stakeholders, UpdateTask, AvailabilityTask, CourseConfig, WatchConfig, IliasConfig
from .ConfigurationStrings import ConfigurationStrings
