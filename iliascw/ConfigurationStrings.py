#!/usr/bin/env python3

class ConfigurationStrings():
    DOMAIN = "Domain"
    USERNAME = "Username"
    PASSWORD = "Password"
    TASKS = "Tasks"
    SEARCH_OBJECT = "search_object"
    SEARCH_STRING = "search_string"
    COURSES = "courses"
    ILIAS = "ilias"
    TYPE = "type"
